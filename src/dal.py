from src.tools.process import processing
from src.tools import logger
from src.tools.error import Error,ErrorType
from src.config import *
from src import scrape as sc
from src.classes.attraction import *

import json
from datetime import datetime
from skill_core_tools.downloader.api_downloader import DataApiDownloader, GaiaConnectionData
from skill_core_tools.uploader.api_uploader import DataApiUploader
import os

def uploadFile(attractions):
    week = int(datetime.now().strftime("%V"))+1
    year = datetime.now().strftime("%Y")
    filename = DATA_API_FILE.format(week = week, year = year)
    jsonData = [attraction.toDict() for attraction in attractions]
    jsonData = json.dumps(jsonData)
    DataApiUploader.upload(DataApiUploader.create(GaiaConnectionData()), DATA_API_FOLDER, filename, jsonData)

def downloadFile():
    week = datetime.now().strftime("%V")
    year = datetime.now().strftime("%Y")
    filename = DATA_API_FILE.format(week = week, year = year)
    path = DataApiDownloader.download(DataApiDownloader.create(GaiaConnectionData()), DATA_API_FOLDER, filename)[0]
    attractions = readFromFile(path)

    try:
        attractions = _filterAttractions(attractions)
    except Exception as e:
        logger.add(Error(ErrorType.FilterError, 'red', str(e)))

    jsonData = [attraction.toDict() for attraction in attractions]
    jsonData = json.dumps(jsonData)
    return jsonData

def _filterAttractions(attractions):
    links = sc.getOpen()
    print(links)

    filtered_attractions = []
    for attraction in attractions: 
        if attraction.link in links:
            filtered_attractions.append(attraction)
    
    return filtered_attractions

def downloadGeckodriver():
    geckoPath = DataApiDownloader.download(DataApiDownloader.create(GaiaConnectionData()), DATA_API_FOLDER, DATA_API_DRIVER)[0]
    os.chmod(geckoPath, 0o751)

    geckoPath = str(geckoPath).split('/geckodriver')[0]
    geckoPath = os.path.abspath(geckoPath)
    os.environ['PATH'] += os.pathsep + geckoPath
    pass

def writeToFile(attractions):
    processing('Saving to File:', '\r', 1, 0)
    jsonData = [attraction.toDict() for attraction in attractions]
    jsonData = str(jsonData)

    try:
        file = open("src/output/saveFile.json", 'w')
        file.write(jsonData)
        file.close()
    except Exception as e:
        logger.add(Error(ErrorType.FileWritingError, 'red', str(e)))

    processing('Saving to File:', '\n', 1, 1)

def readFromFile(filename):
    processing('Reading from File:', '\r', 1, 0)

    try:
        file = open(filename, 'r')
        jsonData = json.loads(file.read())
        attractions = [Attraction.fromDict(attraction) for attraction in jsonData]
    except Exception as e:
        attractions = list()
        logger.add(Error(ErrorType.FileReadingError, 'red', str(e)))

    processing('Reading from File:', '\n', 1, 1)
    return attractions