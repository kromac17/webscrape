from src.classes.attraction import Attraction, Address, Contact, Regularprice, Locationimage, Event, BusinessHours, CardBenefit, Description, Theme, Region
from src.tools.error import Error, ErrorType
from src.tools import logger
from src.tools.process import processing
from src.config import *
from src import dal


from selenium import webdriver
from selenium.webdriver.common.by import By
    #By Options: ID, XPATH, LINK_TEXT, PARTIAL_LINK_TEXT, NAME, TAG_NAME, CLASS_NAME, CSS_SELECTOR
from selenium.webdriver.firefox.options import Options
from PIL import Image
import requests
import re
from datetime import datetime

def _getDriver(headless):
    #headless option
    options = Options()  
    options.headless = headless

    try: 
        driver = webdriver.Firefox(options=options)
    except:
        try: dal.downloadGeckodriver()
        except Exception as e: logger.add(Error(ErrorType.DownloadError, 'red', str(e)))
        driver = webdriver.Firefox(options=options)

    return driver

def getOpen():
    #driver
    try:
        driver = _getDriver(DRIVER_HEADLESS)
    except Exception as e:
        logger.add(Error(ErrorType.DriverError, 'red', str(e)))
        return

    date = datetime.now().strftime('%d.%m.%Y')
    search = SC_SEARCH_OPEN.format(date=date)
    driver.get(SC_LINK+search)

    #get a list of all links to the attractions
    search_results = driver.find_elements(By.XPATH, "//div[@class='col-sm-6 col-xs-12 search-result']/div[@class='row']/a")
    links = [element.get_attribute('href') for element in search_results]

    driver.quit()
    return links

def getAttractions():
    #driver
    try:
        driver = _getDriver(DRIVER_HEADLESS)
        driver.get(SC_LINK+SC_SEARCH_ALL)
    except Exception as e:
        logger.add(Error(ErrorType.DriverError, 'red', str(e)))
        return

    #get a list of all links to the attractions
    search_results = driver.find_elements(By.XPATH, "//div[@class='col-sm-6 col-xs-12 search-result']/div[@class='row']/a")
    links = [element.get_attribute('href') for element in search_results]

    if len(links) == 0: 
        logger.add(Error(ErrorType.LoadingFailure, 'red', ''))
        return
    
    #get the attraction from every link; Testdata=10
    attractions = []

    if SC_LIMITER>0 and SC_LIMITER<len(links) : links = links[:SC_LIMITER]
    i = 1

    for link in links:
        try:
            driver.get(link)
        except Exception as e:
            logger.add(Error(ErrorType.LinkFailure, 'red', str(e)))

        #process line
        processing('Scraping Attractions:', '\r',len(links), i)
        i += 1

        #check if page is realy loaded
        if driver.current_url == SC_LINK:
            logger.add(Error(ErrorType.LinkFailure, 'red', link))
            continue

        #[print(line.get_attribute('outerHTML'), '\n------------------------------\n') for line in content.find_elements(By.CLASS_NAME, 'row')]
        try:
            id = driver.find_element(By.ID, "save-fav-location").get_attribute('data-location-id')
            try:
                attraction = _getAttraction(driver, id, link)
                attractions.append(attraction)
            except:
                logger.add(Error(ErrorType.AttractionError, 'red', link))
        except:
            logger.add(Error(ErrorType.IdNotFound, 'red', link))

    processing('Scraping Attractions:', '\n',len(links), len(links))
    attractions = sorted(attractions)
    try:
        _insertRegions(SC_LINK, SC_SEARCH_REGION, driver, attractions)
    except Exception as e:
        logger.add(Error(ErrorType.RegionError, 'red', str(e)))
    
    try:
        _insertThemes(SC_LINK, SC_SEARCH_THEME, driver, attractions)
    except Exception as e:
        logger.add(Error(ErrorType.ThemeError, 'red', str(e)))

    driver.quit()
    return attractions

def _getAttraction(driver, id, link):
    content = driver.find_element(By.XPATH, "//div[@id='c268']/div/div")

    #--row1
    row = content.find_elements(By.CLASS_NAME, "row")[0]
    name = row.find_element(By.XPATH, "//div/h1").text
    shortDesc = row.find_element(By.XPATH, "//div/h2").text

    #--row2
    row = content.find_elements(By.CLASS_NAME, "row")[1]
    card_benefit = _getCardBenefit(row)

    regularprices = _getRegularPrices(row)
    services = _getServices(row)

    #--row3
    row = content.find_elements(By.CLASS_NAME, "row")[2]
    locationimage = _getImage(row)

    #--row4
    row = content.find_elements(By.CLASS_NAME, "row")[4] #4 because 3 is a copy of 2
    business_hours = _getBusinessHours(row)

    details = row.find_element(By.CLASS_NAME, "location-details")
    contact = _getContact(details)
    description = _getDescription(details)
    

    #--row5
    row = content.find_elements(By.CLASS_NAME, "row")[7]
    address = _getAddress(row, details)

    #--locationevents
    events = _getEvents(driver, name)
    
    return Attraction(id, link, name, shortDesc, card_benefit, regularprices, locationimage, services, business_hours, contact, description, address, events)

def _getRegions(weblink, driver):
    driver.get(weblink)

    #get what regions are there
    region_form = driver.find_element(By.ID, 'searchform')
    region_form = region_form.find_element(By.CLASS_NAME, 'region-list')
    region_form = region_form.find_element(By.TAG_NAME, 'ul')

    regions = []
    region_list = region_form.find_elements(By.XPATH, "//li/div")
    for region in region_list:
        id = region.find_element(By.TAG_NAME, 'input').get_attribute('id')

        #don't include "Alle Regionen"
        if 'alle' not in id:
            value = region.find_element(By.TAG_NAME, 'input').get_attribute('value')
            name = region.find_element(By.TAG_NAME, 'label').find_elements(By.TAG_NAME, 'span')[1].text

            regions.append(Region(value, id, name))

    return sorted(regions)

def _insertRegions(weblink, region_search, driver, attractions):
    regions = _getRegions(weblink, driver)
    i = 1

    #go through every region and add to attraction
    for region in regions:
        search = region_search.format(id = region.id)
        driver.get(weblink+search)

        #process line
        processing('Scraping Regions:', '\r',len(regions), i)
        i += 1

        #get a list of all links to the attractions
        search_results = driver.find_elements(By.XPATH, "//div[@class='col-sm-6 col-xs-12 search-result']/div[@class='row']/a")
        links = [element.get_attribute('href') for element in search_results]

        #go through all attractions and add region where link fits
        for attraction in attractions:
            if attraction.link in links:
                attraction.region = region

    processing('Scraping Regions:', '\n',len(regions), len(regions))
    pass

def _getThemes(weblink, driver):
    driver.get(weblink)

    #get what regions are there
    theme_form = driver.find_element(By.ID, 'theme')

    themes = []
    theme_list = theme_form.find_elements(By.TAG_NAME, 'div')
    for theme in theme_list:
        id = theme.find_element(By.TAG_NAME, 'input').get_attribute('id')
        value = theme.find_element(By.TAG_NAME, 'input').get_attribute('value')
        name = theme.find_element(By.TAG_NAME, 'label').text

        themes.append(Theme(value, id, name))

    return sorted(themes)

def _insertThemes(weblink, theme_search, driver, attractions):
    themes = _getThemes(weblink, driver)
    i = 1

    #go through every theme and add to attraction
    for theme in themes:
        search = theme_search.format(id = theme.id)
        driver.get(weblink+search)

        #process line
        processing('Scraping Themes:', '\r',len(themes), i)
        i += 1

        #get a list of all links to the attractions
        search_results = driver.find_elements(By.XPATH, "//div[@class='col-sm-6 col-xs-12 search-result']/div[@class='row']/a")
        links = [element.get_attribute('href') for element in search_results]

        #go through all attractions and add theme where link fits
        for attraction in attractions:
            if attraction.link in links:
                attraction.theme = theme

    processing('Scraping Themes:', '\n',len(themes), len(themes))
    pass

def _getCardBenefit(row):
    info = row.find_element(By.CLASS_NAME, "location-cardservices").find_element(By.XPATH, "span[@class='field-content big']/p").text
    if re.search('einmalig', info, re.IGNORECASE):
        category = 'Einmalig'
    elif re.search('sooft', info, re.IGNORECASE):
        category = 'Unbegrenzt'
    else:
        category = ''

    try:
        not_included = row.find_element(By.CLASS_NAME, "location-cardservices").find_element(By.CLASS_NAME, "not-included-text").text
    except:
        not_included = None

    return CardBenefit(category, info, not_included)

def _getRegularPrices(row):
    pricelist = row.find_elements(By.XPATH, "//div[@class='col-sm-6 col-xs-12']")[1]
    pricelist = pricelist.find_elements(By.XPATH, "//div[@class='location-regularprice']/span[@class='field-content big']")
    regularprices = []
    for price in pricelist:
        desc = price.text.split(':')[0]
        if desc != '':
            try:
                price = price.find_element(By.CLASS_NAME, 'formated-price').text
                price = price.split('€ ')[1]
                regularprices.append(Regularprice(desc, price))
            except: #there is no price -> can eather be additional info or the price is a word like free
                try: #has the description in it
                    info = price.text.split(': ')[1]
                    if re.search('(ohne|exkl)', price.text, re.IGNORECASE): #is info
                        for regularprice in regularprices:
                            if regularprice.desc == desc:
                                regularprice.info = info
                    else: #is price but means zero
                        regularprices.append(Regularprice(desc, 0))
                except: #has no description, probably points to the last description
                    info = price.text
                    if re.search('(ohne|exkl)', price.text, re.IGNORECASE): #is info
                        regularprices[len(regularprices)-1].info = info
                    else: #is price but means zero
                        regularprices.append(Regularprice(desc, 0))

    return regularprices

def _getServices(row):
    services = row.find_elements(By.XPATH, "//div[@class='col-sm-6 col-xs-12']")[1]
    services = services.find_elements(By.XPATH, "//div[@class='location-additionalservices-icon']/span")

    return [service.get_attribute('title') for service in services]

def _getImage(row):
    image_container = row.find_element(By.XPATH, "//div/div[@class='location-image']/a")
    image_name = image_container.get_attribute('title')
    #inconsistant picture names
    if '©' in image_name:
        copyright = image_name.split('©')[1]
        image_name = image_name.split('©')[0]
    elif '@' in image_name:
        copyright = image_name.split('@')[1]
        image_name = image_name.split('@')[0]
    elif ':' in image_name:
        copyright = image_name.split(':')[1]
        image_name = image_name.split(':')[0]

    image_link = image_container.get_attribute('href')
    image = Image.open(requests.get(image_link, stream=True).raw)
    image = None

    return Locationimage(image_name, copyright, image, image_link)

def _getBusinessHours(row):
    lines = row.find_element(By.XPATH, "//div[@class='col-md-4 col-md-pull-8 location-sidebar']/div[@class='location-openingtimes']")
    lines = lines.find_elements(By.TAG_NAME, 'p')

    text = ''
    for line in lines:
        if line.text != '':
            text += line.text + '\n\n'

    #remove strange hyphen
    text = list(text)
    y = 0
    for char in text:
        if ord(char) == 8211:
            text[y] = '-'
        y += 1
    text = "".join(text)

    results = REGEX_BUSINESS_HOUR.findall(text)
    #if no date or time is given, search if open the hole year
    if not results:
        results = REGEX_DURCHGEHEND.findall(text)

    #if no date or time is given, get link
    if not results:
        results = REGEX_LINK.findall(text)

    return BusinessHours(text, [match[0].strip().replace('\n', ' ') for match in results])

def _getContact(details):
    contact = details.find_elements(By.XPATH, "//div[@class='location-contact']/a")
    valid_phones = []
    valid_mails = []
    website = ''

    for info in contact:
        href = info.get_attribute('href')[:3]

        if href == 'tel':
            valid_phones = REGEX_PHONE.findall(info.text)
            valid_phones = _deleteDoubles(valid_phones)
            pass
        elif href == 'mai':
            valid_mails = REGEX_MAIL.findall(info.text)
            valid_mails = _deleteDoubles(valid_mails)
            pass
        elif href == 'htt':
            website = info.get_attribute('href')
            pass

    return Contact(valid_phones, valid_mails, website)

def _getDescription(details):
    description = Description(details.find_element(By.CLASS_NAME, "location-comment").text)
    try:
        description.arrival = details.find_element(By.CLASS_NAME, "location-directions").text + '\n'
    except:
        pass
    try:
        description.tips = details.find_element(By.CLASS_NAME, "location-tips").text + '\n'
    except:
        pass

    return description

def _getAddress(row, details):
    address = details.find_element(By.XPATH, "//div[@class='location-contact']").text.split('Tel.:')[0].split('Kontakt')[1]
    lines = address.split('\n')
    lines = lines[1:]

    name = lines[0]
    if lines[1][0].isdigit():
        street = ''
        zip_code = lines[1][:4]
        place = lines[1][5:]
    else:
        street = lines[1]
        zip_code = lines[2][:4]
        place = lines[2][5:]

    coordinates = row.find_element(By.XPATH, "div/div/form/input")
    coordinates = coordinates.get_attribute('value')

    return Address(name, zip_code, place, street, coordinates)

def _getEvents(driver, attraction_name):
    event_list = driver.find_elements(By.XPATH, "//div[@id='location-events']/a")
    events = list()

    if event_list and not event_list[0].get_attribute('innerHTML').strip():
        event_list = driver.find_elements(By.XPATH, "//div[@id='location-events']/div")

    for event in event_list:
        try: #there are two different ways the website is constructed at the event thing
            content = event.find_element(By.XPATH, "div/div").find_elements(By.CLASS_NAME, 'row')[0]
            name = content.find_element(By.XPATH, "div/h4").text
            link = event.get_attribute('href')

            content = event.find_element(By.XPATH, "div/div").find_elements(By.CLASS_NAME, 'row')[1]
            column = content.find_elements(By.TAG_NAME, 'div')[2].text
            eventtimes = REGEX_EVENT_TIME.findall(column)
            begin = datetime.strptime(eventtimes[0], '%d.%m.%Y %H:%M Uhr')
            end = datetime.strptime(eventtimes[1], '%d.%m.%Y %H:%M Uhr')

            if 'Veranstaltungsort: ' in column:
                details = column.split('Veranstaltungsort: ')[0]
                details = [detail.strip() for detail in details.split('✔')][1:]
                place = column.split('Veranstaltungsort: ')[1].strip()
            else:
                details = [detail.strip() for detail in column.split('✔')][1:]
                place = attraction_name

            description = content.find_elements(By.TAG_NAME, 'div')[3].find_element(By.TAG_NAME, 'div').text
            events.append(Event(name, link, description, place, begin, end, details))
        except:
            name = event.find_element(By.XPATH, "div/a/div/div/h4").text
            link = event.find_element(By.TAG_NAME, 'a').get_attribute('href')

            column = event.find_element(By.XPATH, "div/div/a").text
            regex_datetime = re.compile(REGEX_EVENT_TIME)
            eventtimes = regex_datetime.findall(column)
            begin = datetime.strptime(eventtimes[0], '%d.%m.%Y %H:%M Uhr')
            end = datetime.strptime(eventtimes[1], '%d.%m.%Y %H:%M Uhr')

            if 'Veranstaltungsort: ' in column:
                details = column.split('Veranstaltungsort: ')[0]
                details = [detail.strip() for detail in details.split('✔')][1:]
                place = column.split('Veranstaltungsort: ')[1].strip()
            else:
                details = [detail.strip() for detail in column.split('✔')][1:]
                place = attraction_name

            description = event.find_element(By.XPATH, "div/div/div/div").text
            events.append(Event(name, link, description, place, begin, end, details))
            logger.add(Error(ErrorType.EventInfo, 'yellow', name))
    
    return events

def _deleteDoubles(mylist):
    mylist = list(dict.fromkeys(mylist))
    return mylist