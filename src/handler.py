from src import scrape as sc
from src import dal
from src.config import *
from src.tools import logger
from src.tools.error import ErrorType, Error

import time
from datetime import datetime

def scrape():
    attractions = None
    try: attractions = sc.getAttractions()
    except Exception as e: logger.add(Error(ErrorType.ScrapingError, 'red', str(e)))

    return attractions

def downloadData():
    jsonData = None
    try: jsonData = dal.downloadFile()
    except Exception as e: logger.add(Error(ErrorType.DownloadError, 'red', str(e)))
    
    return jsonData

def uploadData(attractions):
    try: dal.uploadFile(attractions)
    except Exception as e:
        week = int(datetime.now().strftime("%V"))+1
        year = datetime.now().strftime("%Y")
        filename = DATA_API_FILE.format(week = week, year = year)
        
        logger.add(Error(ErrorType.UploadError, 'red', f'File {filename} already exists'))
        raise TypeError(f"File {filename} already exists!")

def evaluate(payload: dict, context: dict) -> dict:
    start = time.time()
    request = payload["request"]
    
    try:
        if request == "get":
            response = downloadData()
            if response == None:
                response = 'Failed'
                raise TypeError("Downloaded Data equals None!")
        elif request == "scrape":
            attractions = scrape()
            response = 'Done'
            if attractions == None:
                response = 'Failed'
                raise TypeError("Scraped Data equals None!")
            else:
                uploadData(attractions)
        else:
            response = f'Request cannot be fulfilled: {request}'
    except Exception as e:
        error = Error(ErrorType.UnknownError, 'magenta', f'{str(type(e))}: {str(e)}')
        logger.add(error)
        response = error.logString()

    seconds = time.time() - start
    return {'response': response, 'errors': str(logger.output()), 'seconds': int(seconds)}