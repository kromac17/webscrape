import re

DATABASE_HOST = 'localhost'
DATABASE_USER = 'python'
DATABASE_NAME = 'attractions'
DATABASE_PASSWORD = '5Z3h72VK1vwVKLHHUuqJ3IYf110WSuW0LdmDVCzTPPgNNJjs2wAINm7ferIMGIOM'

DATA_API_FOLDER = 'SteiermarkCard_Data'
DATA_API_FILE = 'SteiermarkCard_Data_{week}_{year}.json'
DATA_API_DRIVER = 'geckodriver'

DRIVER_HEADLESS = True
SC_LIMITER = 0
SC_LINK = 'https://www.steiermark-card.net/ausflugsziele/'
SC_SEARCH_ALL = '?tx_stores_pi1%5Bpage%5D=&tx_stores_pi1%5BitemsPerPage%5D=all&id=37&L=&tx_stores_pi1%5Baction%5D=filterForm'
SC_SEARCH_REGION = '?tx_stores_pi1%5Bpage%5D=&tx_stores_pi1%5BitemsPerPage%5D=all&tx_stores_pi1%5Bregions%5D%5B%5D={id}&id=50&L=&tx_stores_pi1%5Baction%5D=search'
SC_SEARCH_THEME = '?tx_stores_pi1%5Bpage%5D=&tx_stores_pi1%5BitemsPerPage%5D=all&tx_stores_pi1%5Bthemes%5D%5B%5D={id}&id=50&L=&tx_stores_pi1%5Baction%5D=search'
SC_SEARCH_OPEN = '?tx_stores_pi1%5Bpage%5D=&tx_stores_pi1%5BitemsPerPage%5D=all&tx_stores_pi1%5Bdate%5D={date}&tx_stores_pi1%5Bdate-proximity%5D%5B%5D=1&id=50&L=&tx_stores_pi1%5Baction%5D=search'

JSON_DATE_FORMAT = '%d.%m.%Y, %H:%M'

_months = '((Jan|Jän|Feb|Mär|Apr|Mai|Jun|Jul|Aug|Sept|Okt|Nov|Dez)\.?(uar|ner|er|ruar|z|il|i|ust|ember|ober)?)'
_special_months = '(Schulbeginn|Schulferien|Ostern|Sommerferien|Wintereinbruch|ganzjährig)'
_months_as_nums = '\.\s?0?(1|2|3|4|5|6|7|8|9|10|11|12)(?!\d)\.?'
_days_of_week = '((Mo|Di|Mi|Do|Fr|Sa|So)(\.|\s|(n|ens|tt|nners|ei|ms|nn)(tag|woch)))'
_special_days_of_week = '(Feier|Ferien|Fenster|Wochenende)(tag)?(e)?'
_special_days = 'Anfang|Ende|Mitte|Mitte/Ende'
_speziefisch_info = 'zusätzlich|geöffnet|(nur\s)?(mit|gegen)\sVoranmeldung'

PATTERN_MAIL = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
PATTERN_PHONE = '\d{4,5}\s?/?\s?\d+\s?\w?\d*'
PATTERN_LINK = 'www\.[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}'
PATTERN_LINK = '((www\.[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}))'
PATTERN_MONTH = f'\s?({_months}|{_special_months}|{_months_as_nums})\s?'
PATTERN_DAY_OF_WEEK = f'\s?(Advent)?({_days_of_week}|{_special_days_of_week})\s?'
PATTERN_DAY = '\s?('+_special_days+'|\d{1,2}\.?)\s?'
PATTERN_TIME = '\s?\d{1,2}(\.\d{2})?(\sUhr)?\s?'
PATTERN_YEAR = '\s?\d{4}\s?'
PATTERN_TIMESPAN = f'\s?({PATTERN_TIME})\s?(\-|bis|\,|und)\s?({PATTERN_TIME})\s?'
PATTERN_EVENT_TIME = '\d{2}\.\d{2}\.\d{4}\s\d{2}:\d{2}\sUhr'
PATTERN_TEXT = '[-a-zA-Zäöü0-9@:%\!_\+~#?&//=]'
PATTERN_BIS = '\s?(bis(\seinschließlich)?|-|–)\s?'
PATTERN_ENUMERATION = '\s?(\,|und|&|\()\s?'
PATTERN_GESCHLOSSEN = f'\s?(Ruhetag|geschlossen)\s?'
DURCHGEHEND_PATTERN = f'\s?(jederzeit|durchgehend|immer)\s?'
PATTERN_BINDEGLIED = '\s?(jeden|sowie|und|jeweils|bereits|von|wieder|auch|an|zwischen|Führungen)(\sam)?\s?'
PATTERN_DATE_SECOND_PART = f'({PATTERN_BIS}|{PATTERN_ENUMERATION})(zum)?({PATTERN_DAY})?({PATTERN_MONTH})({PATTERN_YEAR})?'
PATTERN_DATE_FULL = f'\(?({PATTERN_DAY})?({PATTERN_MONTH})\:?({PATTERN_DATE_SECOND_PART})*\)?\,?'
PATTERN_DATE_HALF = f'({PATTERN_DAY})({PATTERN_DATE_SECOND_PART})'
PATTERN_TIME_TIMESPAN = f'\s?((ab|um)\s)?({PATTERN_TIME}\sUhr|{PATTERN_TIMESPAN})\s?'
PATTERN_DAY_OF_WEEK_INFO = f'\s?(({PATTERN_DAY_OF_WEEK}))((({PATTERN_BIS}|{PATTERN_ENUMERATION})({PATTERN_DAY_OF_WEEK})))*(\:?\s{PATTERN_TIME_TIMESPAN})?\s?'
PATTERN_DAY_INFO = f'\s?\:?({PATTERN_BINDEGLIED})?(\(?{PATTERN_DAY_OF_WEEK_INFO}\)?|(täglich|tägl\.)(\svon\s)?\,?((?!\s\D)\s{PATTERN_TIME_TIMESPAN})?|ganzjährig\sgeöffnet|{PATTERN_TIME_TIMESPAN})({PATTERN_GESCHLOSSEN})?\,?\s?'
PATTERN_INFO = f'\s?(bei\s(\w+)|\(({PATTERN_TEXT}*\s?)+\)|\-\s([a-zA-Z\!\?]+\s)+|{_speziefisch_info})\s?'
PATTERN_BEREICH = '\s?(('+PATTERN_TEXT+'+)(?!\n)\s?){1,5}\:\s?'

PATTERN_DATE = f'(ab)?({PATTERN_DATE_HALF}|{PATTERN_DATE_FULL})\:?({PATTERN_INFO})?'
PATTERN_TIME_INFO = f'({PATTERN_DAY_INFO})*({PATTERN_INFO})?'
PATTERN_FINAL_DATE = f'({PATTERN_BEREICH})?{PATTERN_DATE}{PATTERN_TIME_INFO}'
PATTERN_FINAL_TIME = f'({PATTERN_BEREICH})?({PATTERN_DAY_INFO})+({PATTERN_INFO})?'
PATTERN_BUSINESS_HOURS = f'({PATTERN_FINAL_DATE}|{PATTERN_FINAL_TIME})'

REGEX_PHONE = re.compile(PATTERN_PHONE)
REGEX_MAIL = re.compile(PATTERN_MAIL)
REGEX_EVENT_TIME = re.compile(PATTERN_EVENT_TIME)
REGEX_LINK = re.compile(PATTERN_LINK, re.IGNORECASE)
REGEX_DURCHGEHEND = re.compile(DURCHGEHEND_PATTERN, re.IGNORECASE)
REGEX_BUSINESS_HOUR = re.compile(PATTERN_BUSINESS_HOURS, re.IGNORECASE)