from src.tools.error import Error
from datetime import datetime
import os

errors = list()

def add(error:Error):
    errors.append(error)
    pass

def output():
    if len(errors) == 0: return

    path = os.getcwd()
    os.chdir("..")
    path = os.path.join(path, 'output\\webscrapelog.log')
    file = open(path, 'a')
    
    for error in errors:
        file.write(str(datetime.now()) + ' ' + error.logString() +'\n')
        print(str(error))
    file.close

    error_list = [e.logString() for e in errors]
    errors.clear()
    
    return error_list