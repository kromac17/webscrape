from termcolor import colored
import time

processes = list()

def processing(text, end, length, i):
    #save time when process started
    if text not in [p.name for p in processes]:
        processes.append(Process(text, time.time()))

    proz = i/length*100/2

    line = ''
    for x in range(50):
        if x <= proz: line += '▮'
        else: line += '▯'
    
    #print and clear terminal
    if end == '\n':
        timestamp = processes[len(processes)-1].timestamp
        seconds = time.time() - timestamp
        print(colored(f'{text:22s} {line} - {(proz*2):3.0f} % in {seconds:3.0f} sec', 'green'), end=end)
    else:
        print(colored(f'{text:22s} {line} - {(proz*2):3.0f} %', 'green'), end=end)
    pass

class Process():
    def __init__(self, name, timestamp):
        self.name = name
        self.timestamp = timestamp
        pass