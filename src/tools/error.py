from enum import Enum
from termcolor import colored

class ErrorType(Enum):
    LoadingFailure = 'ERROR! Page loading failed'
    LinkFailure = 'WARNING! Link does not load'
    IdNotFound = 'WARNING! Page did not load correctly'
    DatabaseConnectionError = 'ERROR! Connection to database failed'
    DatabaseSetupError = 'ERROR! Database setup failed'
    DatabaseInsertError = 'ERROR! Database insert failed'
    EventInfo = 'INFO! Event constructed different'
    AttractionError = 'WARNING! Attraction failed to scrape'
    ThemeError = 'WARNING! Themes failed to scrape'
    RegionError = 'WARNING! Regions failed to scrape'
    AttractionDuplicateKey = 'WARNING! Attraction already exists'
    FileReadingError = 'ERROR! Reading file was unsuccessful'
    FileWritingError = 'ERROR! Writing to file was unsuccessful'
    Info = 'INFO'
    UnknownError = 'ERROR! Something bad must have happened'
    DriverError = 'ERROR! Webdriver failed'
    DoubleIDError = 'WARNING! Deleting Attraction for double id'
    DownloadError = 'ERROR! Download failed'
    UploadError = 'ERROR! Upload failed'
    FilterError = 'WARNING! Filter attractions failed'
    ScrapingError = 'ERROR! Scraping failed!'

class Error():
    def __init__(self, error:ErrorType, color, info=None):
        self.error = error
        self.color = color
        self.info = info
        pass

    def __str__(self):
        out = colored(self.error.value, self.color)
        if self.info != None: out += ' -> ' + str(self.info)

        return out
    
    def logString(self):
        out = self.error.value
        if self.info != None: out += ' -> ' + str(self.info)

        return out