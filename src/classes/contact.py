class Contact(object):
    def __init__(self, phone_numbers, mail_addresses, website):
        self.phone_numbers = phone_numbers
        self.mail_addresses = mail_addresses
        self.website = website
        pass

    def __str__(self):
        return '[' + str(self.phone_numbers) + ', ' + str(self.mail_addresses) + ', ' + self.website + ']'

    def toDict(self):
        dictionary = {
            "phone_numbers": self.phone_numbers,
            "mail_addresses": self.mail_addresses,
            "website": self.website
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        phone_numbers = dictionary["phone_numbers"]
        mail_addresses = dictionary["mail_addresses"]
        website = dictionary["website"]
        
        return Contact(phone_numbers, mail_addresses, website)