from classes.address import Address
from classes.businesshours import BusinessHours
from classes.cardbenefit import CardBenefit
from classes.contact import Contact
from classes.description import Description
from classes.event import Event
from classes.locationimage import Locationimage
from classes.region import Region
from classes.theme import Theme
from classes.regularprice import Regularprice
from typing import List

class Attraction(object):
    def __init__(self, id, link, name, shortDesc, card_benefit:CardBenefit, regularprices:List[Regularprice], locationimage:Locationimage, services, business_hours:BusinessHours, contact:Contact, description:Description, address:Address, events:List[Event], region:Region=None, theme:Theme=None):
        self.id = id
        self.link = link
        self.name = name
        self.shortDesc = shortDesc
        self.card_benefit = card_benefit
        self.regularprices = regularprices
        self.locationimage = locationimage
        self.services = services
        self.business_hours = business_hours
        self.contact = contact
        self.description = description
        self.address = address
        self.events = events
        self.region = region
        self.theme = theme
        pass

    def __eq__(self, other):
        return self.id == self.id

    def __lt__(self, other):
        return self.id < other.id

    def __str__(self):
        out = 'Identity: ' + self.id + '\n'
        out += 'Name: '+ self.name + '\n'
        out += 'Short Description: ' + self.shortDesc + '\n'
        out += 'Card Benefit: ' + str(self.card_benefit) + '\n'

        out += 'Regularprices: ['
        for regularprice in self.regularprices:
            out += str(regularprice) + ', '
        out += '] \n'

        out += 'Image: ' + str(self.locationimage) + '\n'

        out += 'Services: ['
        for service in self.services:
            out += service + ', '
        out += '] \n'
        
        out += 'Business hours: ' + str(self.business_hours) + '\n'
        out += 'Contact: ' + str(self.contact) + '\n'
        out += 'Description: ' + str(self.description) + '\n'
        out += 'Address: ' + str(self.address) + '\n'
        out += 'Region: ' + str(self.region) + '\n'
        out += 'Theme: ' + str(self.theme) + '\n'
        
        out += 'Events: ['
        for event in self.events:
            out += event + ', '
        out += '] \n'

        return out

    def toDict(self):
        dictionary = {
            "id": self.id,
            "link": self.link,
            "name": self.name,
            "shortDesc": self.shortDesc,
            "card_benefit": self.card_benefit.toDict(),
            "regularprices": [regularprice.toDict() for regularprice in self.regularprices],
            "locationimage": self.locationimage.toDict(),
            "services": self.services,
            "business_hours": self.business_hours.toDict(),
            "contact": self.contact.toDict(),
            "description": self.description.toDict(),
            "address": self.address.toDict(),
            "events": [event.toDict() for event in self.events],
            "region": self.region.toDict(),
            "theme": self.theme.toDict()
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        id = dictionary["id"]
        link = dictionary["link"]
        name = dictionary["name"]
        shortDesc = dictionary["shortDesc"]
        card_benefit = CardBenefit.fromDict(dictionary["card_benefit"])
        regularprices = [Regularprice.fromDict(regularprice) for regularprice in dictionary["regularprices"]]
        locationimage = Locationimage.fromDict(dictionary["locationimage"])
        services = dictionary["services"]
        business_hours = BusinessHours.fromDict(dictionary["business_hours"])
        contact = Contact.fromDict(dictionary["contact"])
        description = Description.fromDict(dictionary["description"])
        address = Address.fromDict(dictionary["address"])
        events = [Event.fromDict(event) for event in dictionary["events"]]
        region = Region.fromDict(dictionary["region"])
        theme = Theme.fromDict(dictionary["theme"])
        
        return Attraction(id, link, name, shortDesc, card_benefit, regularprices, locationimage, services, business_hours, contact, description, address, events, region, theme)

"""
- Attraction
  - id > INT
  - link > STRING
  - name > STRING
  - shortDesc > STRING
  - card_benefit > CARDBENEFIT
    - category > STRING
    - info > STRING
    - not_included > STRING
  - regularprices > LIST(REGULARPRICE)
    - category > STRING
    - price > INT
    - info > STRING
  - locationimage > LOCATIONIMAGE
    - name > STRING
    - copyright > STRING
    - image > PILLOW.IMAGE
    - link > STRING
  - services > LIST(STRING)
  - business_hours > BUSINESSHOURS
    - text > STRING
    - matches > LIST(STRING)
  - contact > CONTACT
    - phone_numbers > LIST(STRING)
    - mail_addresses > LIST(STRING)
    - website > STRING
  - description > DESCRIPTION
    - description > STRING
    - tips > STRING
    - arrival > STRING
  - address > ADDRESS
    - name > STRING
    - zip_code > STRING
    - place > STRING
    - street > STRING
    - coordinates > STRING
  - region > REGION
    - id > INT
    - value > STRING
    - name > STRING
  - theme > THEME
    - id > INT
    - value > STRING
    - name > STRING
  - events > LIST(EVENT)
    - name > STRING
    - link > STRING
    - description > STRING
    - place > STRING
    - begin > DATETIME
    - end > DATETIME
    - details > LIST(STRING)
"""
