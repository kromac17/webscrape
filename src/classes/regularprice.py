class Regularprice(object):
    def __init__(self, category, price, info=None):
        self.category = category
        self.price = price
        self.info = info
        pass

    def __str__(self):
        return '['+self.category+': '+ str(self.price) +']'

    def toDict(self):
        dictionary = {
            "category": self.category,
            "price": self.price,
            "info": self.info
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        category = dictionary["category"]
        price = dictionary["price"]
        info = dictionary["info"]
        
        return Regularprice(category, price, info)