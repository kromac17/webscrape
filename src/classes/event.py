from datetime import datetime

JSON_DATE_FORMAT = '%d.%m.%Y, %H:%M'

class Event(object):
    def __init__(self, name, link, description, place, begin, end, details):
        self.name = name
        self.link = link
        self.description = description
        self.place = place
        self.begin = begin
        self.end = end
        self.details = details
        pass

    def __str__(self):
        return f'{self.name} : {str(self.begin)} - {str(self.end)} \n{self.link}\n{self.place}\n{self.details}\n{self.description}'

    def toDict(self):
        dictionary = {
            "name": self.name,
            "link": self.link,
            "description": self.description,
            "place": self.place,
            "begin": self.begin.strftime(JSON_DATE_FORMAT),
            "end": self.end.strftime(JSON_DATE_FORMAT),
            "details": self.details
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        name = dictionary["name"]
        link = dictionary["link"]
        description = dictionary["description"]
        place = dictionary["place"]
        begin = datetime.strptime(dictionary["begin"], JSON_DATE_FORMAT)
        end = datetime.strptime(dictionary["end"], JSON_DATE_FORMAT)
        details = dictionary["details"]
        
        return Event(name, link, description, place, begin, end, details)