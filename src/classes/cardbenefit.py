class CardBenefit(object):
    def __init__(self, category, info, not_included):
        self.category = category
        self.info = info
        self.not_included = not_included
        pass

    def __str__(self):
        return self.category
    
    def toDict(self):
        dictionary = {
            "category": self.category,
            "info": self.info,
            "not_included": self.not_included
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        category = dictionary["category"]
        info = dictionary["info"]
        not_included = dictionary["not_included"]
        
        return CardBenefit(category, info, not_included)