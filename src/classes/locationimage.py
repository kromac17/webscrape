class Locationimage(object):
    def __init__(self, name, copyright, link):
        self.name = name
        self.copyright = copyright
        self.link = link
        pass

    def __str__(self):
        return self.name + ' © ' + self.copyright

    def toDict(self):
        dictionary = {
            "name": self.name,
            "copyright": self.copyright,
            "link": self.link
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        name = dictionary["name"]
        copyright = dictionary["copyright"]
        link = dictionary["link"]
        
        return Locationimage(name, copyright, link)