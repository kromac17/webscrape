class Theme(object):
    def __init__(self, id, value, name):
        #in database id will be value and value will be id
        self.id = id
        self.value = value
        self.name = name
        pass

    def __eq__(self, other):
        return self.value == self.value

    def __lt__(self, other):
        return self.value < other.value

    def __str__(self):
        return self.value + ' - ' + self.id + ', ' + self.name
    
    def toDict(self):
        dictionary = {
            "id": self.id,
            "value": self.value,
            "name": self.name
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        id = dictionary["id"]
        value = dictionary["value"]
        name = dictionary["name"]
        
        return Theme(id, value, name)