class Description(object):
    def __init__(self, description, tips=None, arrival=None):
        self.description = description
        self.tips = tips
        self.arrival = arrival
        pass

    def __str__(self):
        return f'{self.description}\n{self.tips}\n{self.arrival}'

    def toDict(self):
        dictionary = {
            "description": self.description,
            "tips": self.tips,
            "arrival": self.arrival
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        description = dictionary["description"]
        tips = dictionary["tips"]
        arrival = dictionary["arrival"]
        
        return Description(description, tips, arrival)