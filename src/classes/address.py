class Address(object):
    def __init__(self, name, zip_code, place, street, coordinates):
        self.name = name
        self.zip_code = zip_code
        self.place = place
        self.street = street
        self.coordinates = coordinates
        pass

    def __str__(self):
        return f'{self.name} - {self.zip_code}'

    def toDict(self):
        dictionary = {
            "name": self.name,
            "zip_code": self.zip_code,
            "place": self.place,
            "street": self.street,
            "coordinates": self.coordinates
        }
        
        return dictionary
    
    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        name = dictionary["name"]
        zip_code = dictionary["zip_code"]
        place = dictionary["place"]
        street = dictionary["street"]
        coordinates = dictionary["coordinates"]
        
        return Address(name, zip_code, place, street, coordinates)
