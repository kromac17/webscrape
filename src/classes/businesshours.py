class BusinessHours(object):
    def __init__(self, text, matches):
        self.text = text
        self.matches = matches
        pass

    def __str__(self):
        return f'{self.text}\n-----\n{self.matches}\n--------------------------'

    def toDict(self):
        dictionary = {
            "text": self.text,
            "matches": self.matches
        }
        
        return dictionary

    @staticmethod
    def fromDict(dictionary):
        if(dictionary==None): return None
        text = dictionary["text"]
        matches = dictionary["matches"]
        
        return BusinessHours(text, matches)