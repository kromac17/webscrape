from src import handler, dal
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import os
from unittest import TestCase

class IntegrationDemoTest(TestCase):
    def test_case_driver(self):
        options = Options()  
        options.headless = True

        os.chdir("..")
        os.chdir("..")
        
        path = '/tmp/geckodriver'
        downloaded_path = dal.downloadGeckodriver()

        driver = webdriver.Firefox(executable_path=path, options=options)
        link = "https://www.google.com/"
        driver.get(link)
        current = driver.current_url
        driver.quit()

        self.assertEqual(link, str(current))

    def test_case_scrape(self):
        payload: dict = {"request": 'scrape'}
        context: dict = {}

        result: dict = handler.evaluate(payload, context)

        self.assertEqual('Done', result["response"])