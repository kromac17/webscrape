from unittest import TestCase
from src import handler

class DemoTest(TestCase):
    def test_case_wrong_request(self):
        payload: dict = {"request": 'nothing'}
        context: dict = {}

        result: dict = handler.evaluate(payload, context)

        self.assertEqual('nothing', result["response"].split(': ')[1])
